/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   TestPablo.c
 * Author: Pablo
 *
 * Created on 20 de Abril de 2018, 11:58
 */


#include "tm.h"
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
void testes_ERRO_MZ(){
     uint32_t  r;
     struct nivel n;
		n.tempo_acesso = 0;
		n.taxa_falta = 0; // 95% hit
   r=tempo_medio(1,&n,NULL);
    if(r==0){
   printf("\n Valor zero na memoria");
   printf("%"PRIu32,r);
   }else {
       printf("!!ERROR!!");
   }
}
void testes_ERRO_TBLmaiorM(){
    struct nivel n[2];
		n[0].tempo_acesso = 10;
		n[0].taxa_falta = 50000; 
		n[1].tempo_acesso = 100;
		n[1].taxa_falta = 0; 
		struct nivel tlb;
		tlb.tempo_acesso = 100;
		tlb.taxa_falta = 50000; 
		

   uint32_t  r;
   r=tempo_medio(2,n,&tlb);
   
    if(r == 0){
   printf("\n falha sem memoria");
   printf("%"PRIu32,r);
   }else {
       printf("!!ERROR!!");
   }
}
void testes_ERRO_NiveismMenores(){
    struct nivel n[3];
		n[0].tempo_acesso = 1000;
		n[0].taxa_falta = 50000; 
		n[1].tempo_acesso = 10;
		n[1].taxa_falta = 100000;
                n[2].tempo_acesso = 100;
		n[2].taxa_falta = 0; 
		struct nivel tlb;
		tlb.tempo_acesso = 1;
		tlb.taxa_falta = 50000; 
		
    uint32_t  r;
   r=tempo_medio(2,n,&tlb);
    if(r ==0 ){
   printf("\n falha sem memoria");
   printf("%"PRIu32,r);
   }else {
       printf("!!ERROR!!");
   }
    
}
void testes_MeNULL(){
    	
   uint32_t  r;
   r=tempo_medio(0,NULL,NULL);
   if(r==0){
   printf("\n falha sem memoria");
   printf("%"PRIu32,r);
   }else {
       printf("!!ERROR!!");
   }
}
void testes_L1(){
    	struct nivel n[2];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
		n[1].tempo_acesso = 100;
		n[1].taxa_falta = 0; 
		
		

   uint32_t  r;
   r=tempo_medio(2,n,NULL);
     printf("\n  teste de 1 nives  \n");        
   printf("%"PRIu32,r);
    
}
void testes_L1_TLB(){
    struct nivel n[2];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
		n[1].tempo_acesso = 100;
		n[1].taxa_falta = 0; 
		struct nivel tlb;
		tlb.tempo_acesso = 1;
		tlb.taxa_falta = 50000; 
		

   uint32_t  r;
   r=tempo_medio(2,n,&tlb);
     printf("\n  teste de 1 nives mais TLB \n");        
   printf("%"PRIu32,r);
    
}
void testes_L2(){
    struct nivel n[3];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
                n[1].tempo_acesso = 10;
		n[1].taxa_falta = 50000;
		n[3].tempo_acesso = 100;
		n[3].taxa_falta = 0; 
		
		

   uint32_t  r;
   r=tempo_medio(3,n,NULL);
     printf("\n  teste de 2 nives  \n");        
   printf("%"PRIu32,r);
    
}
void testes_L2_TLB(){
     struct nivel n[3];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
                n[1].tempo_acesso = 10;
		n[1].taxa_falta = 50000;
		n[3].tempo_acesso = 100;
		n[3].taxa_falta = 0; 
		struct nivel tlb;
		tlb.tempo_acesso = 1;
		tlb.taxa_falta = 50000; 
		

   uint32_t  r;
   r=tempo_medio(3,n,&tlb);
     printf("\n  teste de 2 nives mais TLB \n");        
   printf("%"PRIu32,r);
    
}
void testes_L3(){
         struct nivel n[4];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
                n[1].tempo_acesso = 10;
		n[1].taxa_falta = 50000;
		n[2].tempo_acesso = 50;
		n[2].taxa_falta = 80000; 
                n[3].tempo_acesso = 100;
		n[3].taxa_falta = 0; 
		
		

   uint32_t  r;
   r=tempo_medio(4,n,NULL);
    printf("\n  teste de 3 nives \n");        
   printf("%"PRIu32,r);
    
}
void testes_L3_TLB(){
     struct nivel n[4];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
                n[1].tempo_acesso = 10;
		n[1].taxa_falta = 50000;
		n[2].tempo_acesso = 50;
		n[2].taxa_falta = 80000; 
                n[3].tempo_acesso = 100;
		n[3].taxa_falta = 0; 
		struct nivel tlb;
		tlb.tempo_acesso = 1;
		tlb.taxa_falta = 50000; 
		

   uint32_t  r;
   r=tempo_medio(4,n,&tlb);
     printf("\n  teste de 3 nives mais TLB \n");        
   printf("%"PRIu32,r);
    
}
void testes_L4(){
    struct nivel n[5];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
                n[1].tempo_acesso = 10;
		n[1].taxa_falta = 50000;
		n[2].tempo_acesso = 50;
		n[2].taxa_falta = 80000; 
                n[3].tempo_acesso = 70;
		n[3].taxa_falta = 80000;
                n[4].tempo_acesso = 100;
		n[4].taxa_falta = 0; 
		
		

   uint32_t  r;
   r=tempo_medio(5,n,NULL);
     printf("\n  teste de 4 nives  \n");        
   printf("%"PRIu32,r);
}
void testes_L4_TLB(){
      struct nivel n[5];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
                n[1].tempo_acesso = 10;
		n[1].taxa_falta = 50000;
		n[2].tempo_acesso = 50;
		n[2].taxa_falta = 80000; 
                n[3].tempo_acesso = 70;
		n[3].taxa_falta = 80000;
                n[4].tempo_acesso = 100;
		n[4].taxa_falta = 0; 
		struct nivel tlb;
		tlb.tempo_acesso = 1;
		tlb.taxa_falta = 50000; 
		

   uint32_t  r;
   r=tempo_medio(5,n,&tlb);
     printf("\n  teste de 4 nives mais TLB \n");        
   printf("%"PRIu32,r);
}
void testes_L5(){
      struct nivel n[5];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
                n[1].tempo_acesso = 10;
		n[1].taxa_falta = 50000;
		n[2].tempo_acesso = 50;
		n[2].taxa_falta = 80000; 
                n[3].tempo_acesso = 70;
		n[3].taxa_falta = 80000;
                n[3].tempo_acesso = 75;
		n[3].taxa_falta = 100000;
                n[4].tempo_acesso = 100;
		n[4].taxa_falta = 0; 
		
    
                 uint32_t  r;
   r=tempo_medio(6,n,NULL);
     printf("\n  teste de 5 nives  \n");        
   printf("%"PRIu32,r);
    
}
void testes_L5_TLB(){
    struct nivel n[5];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
                n[1].tempo_acesso = 10;
		n[1].taxa_falta = 50000;
		n[2].tempo_acesso = 50;
		n[2].taxa_falta = 80000; 
                n[3].tempo_acesso = 70;
		n[3].taxa_falta = 80000;
                n[3].tempo_acesso = 75;
		n[3].taxa_falta = 100000;
                n[4].tempo_acesso = 100;
		n[4].taxa_falta = 0; 
		struct nivel tlb;
		tlb.tempo_acesso = 1;
		tlb.taxa_falta = 50000;
    
                 uint32_t  r;
   r=tempo_medio(6,n,&tlb);
    printf("\n  teste de 5 nives mais TLB \n");        
   printf("%"PRIu32,r);
}
void Bigtestes_L10_TLB(){
    struct nivel n[11];
		n[0].tempo_acesso = 1;
		n[0].taxa_falta = 50000; 
                n[1].tempo_acesso = 10;
		n[1].taxa_falta = 70000;
		n[2].tempo_acesso = 20;
		n[2].taxa_falta = 90000; 
                n[3].tempo_acesso = 30;
		n[3].taxa_falta = 80000;
                n[4].tempo_acesso = 35;
		n[4].taxa_falta = 100000;
                n[5].tempo_acesso = 40;
		n[5].taxa_falta = 50000; 
                n[6].tempo_acesso = 45;
		n[6].taxa_falta = 55000;
		n[7].tempo_acesso = 55;
		n[7].taxa_falta = 88000; 
                n[8].tempo_acesso = 75;
		n[8].taxa_falta = 85000;
                n[9].tempo_acesso = 85;
		n[9].taxa_falta = 10000;
                n[10].tempo_acesso = 100;
		n[10].taxa_falta = 0; 
		struct nivel tlb;
		tlb.tempo_acesso = 1;
		tlb.taxa_falta = 20000;
    
                 uint32_t  r;
   r=tempo_medio(11,n,&tlb);
   printf("\n BIG teste de 11 nives mais TLB \n");        
   printf("%"PRIu32,r);
    
}



int main() {

    testes_ERRO_MZ();
    testes_ERRO_TBLmaiorM();
    testes_ERRO_NiveismMenores();
    testes_MeNULL();
    testes_L1();
    testes_L1_TLB();
    testes_L2();
    testes_L2_TLB();
    testes_L3();
    testes_L3_TLB();
    testes_L4();
    testes_L4_TLB();
    testes_L5();
    testes_L5_TLB();
    Bigtestes_L10_TLB();
    
    return (EXIT_SUCCESS);
}
